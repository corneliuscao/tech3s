package com.tech3s.solution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ex4 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true){
            System.out.println("________________________MENU_____________________");
            System.out.println("1.Hinh vuong N x N");
            System.out.println("2.Hinh tam giac vuong chieu cao N");
            System.out.println("3.Hinh tam giac vuong chieu cao N type2");
            System.out.println("4.Hinh tam giac vuong chieu cao N type3");
            System.out.println("5.Hinh tam giac vuong chieu cao N type4");
            System.out.println("6.Hinh tam giac can chieu cao N");
            System.out.println("Nhap lua chon cua ban");
            int choice = Integer.parseInt(reader.readLine());
            System.out.println("Nhap chieu cao N cua tam giac : ");
            int n = Integer.parseInt(reader.readLine());

            switch (choice) {
                case 1:
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < n; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                    break;
                case 2:
                    for (int i = 1; i <= n; i++) {
                        for (int j = 1; j <= i; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                    break;
                case 3:
                    for (int i = n; i >= 1; i--) {
                        for (int j = 1; j <= i; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                    break;
                case 4:
                    for (int i = 1; i <= n; i++) {
                        for (int k = 1; k <= n - i; k++) {
                            System.out.print(" ");
                        }

                        for (int j = 1; j <= i; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                    break;
                case 5:
                    for (int i = n; i >= 1; i--){
                        for (int k = 1; k <= n - i; k++){
                            System.out.print(" ");
                        }

                        for (int j = 1; j <= i; j++){
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                    break;
                case 6:
                    for (int i = 0; i < n; i++){
                        for (int k = 1; k <= n - i; k++){
                            System.out.print(" ");
                        }

                        for (int j = 0; j < 2*i + 1; j++){
                            System.out.print("*");
                        }

                        System.out.println();
                    }
                    break;
                case 7:
                    return;
            }
        }
    }
}
